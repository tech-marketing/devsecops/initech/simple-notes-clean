# Simple Notes Clean (Security Demo Application) 🔒

Same as [Simple Notes](https://gitlab.com/tech-marketing/devsecops/initech/simple-notes) but with no scanners or deployments. Add the SAST job via configuration UI.

